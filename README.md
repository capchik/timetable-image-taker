# Timetable Image Taker

# Запуск проекта
docker compose build  
docker compose up -d


# Использование
Перейдите на адрес localhost:9001 для просмотра minio 

# Запрос на примере google.com
Возврат PNG без кэширования  
localhost:8800/schedule?url=https://google.com  
  
  
Возврат PNG с сохранением в кэш  
localhost:8800/schedule?url=https://google.com&canCache=True


