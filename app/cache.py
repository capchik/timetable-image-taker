import minio
import urllib
from conf import M_ENDPOINT, M_ACCESS_KEY, M_SECRET_KEY, M_BUCKET_NAME

# Connecting to Minio
minio_client = minio.Minio(
    endpoint=M_ENDPOINT,
    access_key=M_ACCESS_KEY,
    secret_key=M_SECRET_KEY,
    secure=False
)


# Checks if a screenshot from the specified link is in the Minio database.
# Input parameters - url link
def get_cached_schedule(url):
    try:
        url_upd1 = url + '.png'
        url_upd2 = urllib.parse.quote(url_upd1, safe='')
        image_data = minio_client.get_object(M_BUCKET_NAME, url_upd2).read()
        return image_data
    except:
        return None


# Saving a screenshot to the Minio database.
# Input parameters - link, screenshot, resolution
def cache_schedule(url, screenshot, screenshot_size):
    minio_client.put_object(M_BUCKET_NAME, url, screenshot, screenshot_size)
